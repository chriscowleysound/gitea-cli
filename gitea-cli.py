import os
import click
import requests
import json
from terminaltables import AsciiTable
from urllib.parse import urlparse

gitea_url = urlparse(os.environ['GITEA_URL'])
gitea_token = os.environ['GITEA_TOKEN']

api_url = gitea_url.scheme + "://" + gitea_url.netloc + "/api/v1"

headers = {
    "Authorization": "token %s" % gitea_token,
    "accept": "application/json"
}

def get_current_user():
    r = requests.get('%s/user' % api_url, headers=headers)
    return r.json()['username']

@click.group()
def cli():
    pass

@click.group()
def repo():
    pass

@click.command()
def list():
    """Simple program that lists gitea repos"""
    request_url = api_url + "/repos/search"
    OUTPUT = [ ['id', 'owner', 'name', 'html_url'] ]
    repos = requests.get(request_url, headers=headers)
    for repo in repos.json()['data']:
        repo_id   = repo['id']
        name = repo['name']
        owner = repo['owner']['login']
        html_url = repo['html_url']
        OUTPUT.append([repo_id, owner, name, html_url])
        print(owner, name, html_url)

    table = AsciiTable(OUTPUT)
    print(table.table)


@click.command()
@click.argument('name')
def create(name):
    """Create a new repo"""
    OUTPUT = [ ['Web', 'SSH Clone', 'HTML_CLONE'] ]
    username= get_current_user()
    auto_init = True
    description = 'test'
    gitignores = ''
    data = {
        "name": name
    }
    request_url = api_url + "/user/repos"
    r = requests.post(request_url, headers=headers, data=data)
    html_url = r.json()['html_url']
    ssh_url = r.json()['ssh_url']
    clone_url = r.json()['clone_url']
    OUTPUT.append([html_url, ssh_url, clone_url])
    table = AsciiTable(OUTPUT)
    print(table.table)


@click.command()
@click.argument('name')
def delete(name):
    """ Delete a repo"""
    username= get_current_user()
    user = username
    request_url = api_url + "/repos/" + user + "/" + name
    r = requests.delete(request_url, headers=headers)
    if r.status_code == 204:
        click.echo("success")
    else:
        click.echo("failed")

cli.add_command(repo)
repo.add_command(create)
repo.add_command(list)
repo.add_command(delete)

if __name__ == '__main__':
    cli()
